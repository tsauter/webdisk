package structs

import (
	"time"
)

// FileDetails hold all details about the uploaded file
type FileDetails struct {
	MagicCode       string    `json:"MagicCode"`
	ExpiryTimestamp time.Time `json:"ExpiryTimestamp"`
	UploadFilename  string    `json:"UploadFilename"`
	UploadTimestamp time.Time `json:"UploadTimestamp"`
	Uploader        string    `json:"Uploader"`
	UploaderIP      string    `json:"UploaderIP"`
	Checksum        string    `json:"Checksum"`
	Size            int64     `json:"Size"`
}

// SortedFileDetails is a sortable FileDetails array
type SortedFileDetails []FileDetails

func (f SortedFileDetails) Len() int {
	return len(f)
}

func (f SortedFileDetails) Less(i, j int) bool {
	return f[i].UploadTimestamp.Before(f[j].UploadTimestamp)
}

func (f SortedFileDetails) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}
