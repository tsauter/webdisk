package structs

import (
	"sort"
	"strconv"
	"testing"
	"time"
)

var (
	// test fds, sorted by UploadTimestamp!
	testFDs = []FileDetails{
		FileDetails{
			MagicCode:       "0",
			UploadTimestamp: time.Date(2000, 2, 16, 23, 21, 22, 0, time.Local),
		},
		FileDetails{
			MagicCode:       "1",
			UploadTimestamp: time.Date(2016, 11, 6, 11, 21, 0, 0, time.Local),
		},
		FileDetails{
			MagicCode:       "2",
			UploadTimestamp: time.Date(2020, 6, 1, 5, 5, 2, 0, time.Local),
		},
	}
)

func TestFileDetailsSorting(t *testing.T) {
	sort.Sort(SortedFileDetails(testFDs))

	for i := 0; i < len(testFDs); i++ {
		if testFDs[i].MagicCode != strconv.Itoa(i) {
			t.Fatalf("invalid ordering detected: pos %d", i)
		}
	}
}
