package DbManager

import (
	"encoding/json"
	"fmt"
	"gitlab.com/tsauter/webdisk/structs"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
)

const (
	jsonExtension = ".json"
)

// make sure DbManager is always satisfied
var _ DbManager = &LocalDBManager{}

// A LocalDBManager represents the internal state of a local file storage
type LocalDBManager struct {
	storageDir string
}

// LocalDBConfig contains all possible configuration values
type LocalDBConfig struct {
	Directory string `yaml:"Directory"`
}

// NewLocalDBManager creates an initialized LocalDBManager struct
// and returns this structure or an error value.
func NewLocalDBManager(storageDir string) (*LocalDBManager, error) {
	var newlm = LocalDBManager{storageDir: storageDir}

	// make sure that the output directory exists
	stats, err := os.Stat(storageDir)
	if err != nil {
		return &newlm, fmt.Errorf("storage dir does not exist or is not readable")
	}
	if !stats.IsDir() {
		return &newlm, fmt.Errorf("specified storage dir is not a directory")
	}

	return &newlm, nil
}

// StoreFileDetails stores the collected FileDetails in
// the local filesystem.
func (lm *LocalDBManager) StoreFileDetails(fd *structs.FileDetails) error {
	filename := lm.BuildFilenameFromMagic(fd.MagicCode)

	if _, err := os.Stat(filename); err == nil {
		return fmt.Errorf("filename already exists")
	}

	fdJSON, err := json.Marshal(&fd)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename+jsonExtension, fdJSON, 0600)
	if err != nil {
		return err
	}

	return nil
}

// GetFiles return all stored FileDetails from local filesystem.
// The result slice is sorted by data.
func (lm *LocalDBManager) GetFiles(files *structs.SortedFileDetails) error {
	tmpfiles, err := filepath.Glob(lm.storageDir + string(filepath.Separator) + "*" + jsonExtension)
	if err != nil {
		return err
	}

	for _, f := range tmpfiles {
		fd, err := lm.ReadJSONFile(f)
		if err != nil {
			return err
		}
		*files = append(*files, fd)
	}

	sort.Sort(*files)

	return nil
}

// LoadDetailsFromMagiccode fetch FileDetails from local a locale file.
// The filename is the specified magic code.
func (lm *LocalDBManager) LoadDetailsFromMagiccode(magiccode string, fd *structs.FileDetails) error {
	jsonFile, err := ioutil.ReadFile(filepath.Join(lm.storageDir, magiccode+jsonExtension))
	if err != nil {
		return err
	}

	err = json.Unmarshal(jsonFile, &fd)
	if err != nil {
		return err
	}

	return nil
}

// BuildFilenameFromMagic builds a full qualified filename for the give magiccode.
// This contains the uploaded file, not the corresponding JSON file
func (lm *LocalDBManager) BuildFilenameFromMagic(magiccode string) string {
	return filepath.Join(lm.storageDir, magiccode)
}

// ReadJSONFile loads the content of a json file in a new FileDetails struct
func (lm *LocalDBManager) ReadJSONFile(jsonFilename string) (fd structs.FileDetails, err error) {
	jsonFile, err := ioutil.ReadFile(jsonFilename)
	if err != nil {
		return
	}

	err = json.Unmarshal(jsonFile, &fd)
	if err != nil {
		return
	}

	return
}
