package DbManager

import (
	"gitlab.com/tsauter/webdisk/structs"
	"time"
)

var (
	testcases = []struct {
		casename string
		jsonMD5  string
		fd       structs.FileDetails
	}{
		{
			"tc1",
			"9480081073ebeda96ad5eeb93864fd01",
			structs.FileDetails{
				MagicCode:       "123456789",
				ExpiryTimestamp: time.Date(2017, 11, 6, 11, 21, 0, 0, time.Local),
				UploadFilename:  "test.txt",
				UploadTimestamp: time.Date(2016, 11, 6, 11, 21, 0, 0, time.Local),
				Uploader:        "joe",
				UploaderIP:      "5.5.5.5",
				Checksum:        "kjflksjfksksd",
				Size:            23,
			},
		},
		{
			"tc2",
			"5617f4846f6a2f686869702ab16fa9d2",
			structs.FileDetails{
				MagicCode:       "987654321",
				ExpiryTimestamp: time.Date(2001, 2, 16, 23, 21, 22, 0, time.Local),
				UploadFilename:  "test2.txt",
				UploadTimestamp: time.Date(2000, 2, 16, 23, 21, 22, 0, time.Local),
				Uploader:        "marc",
				UploaderIP:      "1.1.1.1",
				Checksum:        "dsjfsjfls",
				Size:            49298,
			},
		},
		{
			"tc3",
			"0613f529d17272452979710df2294092",
			structs.FileDetails{
				MagicCode:       "35353535",
				ExpiryTimestamp: time.Date(2021, 6, 1, 5, 5, 2, 0, time.Local),
				UploadFilename:  "test3.txt",
				UploadTimestamp: time.Date(2020, 6, 1, 5, 5, 2, 0, time.Local),
				Uploader:        "ole",
				UploaderIP:      "2.2.2.2",
				Checksum:        "fkajsflsjfksklsf",
				Size:            3333,
			},
		},
	}
)
