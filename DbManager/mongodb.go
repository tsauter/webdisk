package DbManager

import (
	"gitlab.com/tsauter/webdisk/structs"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"sort"
)

const (
	bucketName     = "webdisk"
	collectionName = "files"
)

// make sure DbManager is always satisfied
var _ DbManager = &MongoDBManager{}

// A MongoDBManager represents the internal state of the a local file storage
type MongoDBManager struct {
	mgoSession *mgo.Session
}

// This structure is used in the configuration file
/*
type MongoDBConfig struct {
	Servers []string `yaml:"Servers"`
}
*/

// NewMongoDBManager creates an initialized MongoDBManager struct
// and returns this structure or an error value.
func NewMongoDBManager(mongodbServer string) (*MongoDBManager, error) {
	var newmm = MongoDBManager{}

	mgoSession, err := mgo.Dial(mongodbServer)
	if err != nil {
		return nil, err
	}
	mgoSession.SetMode(mgo.Monotonic, true)
	newmm.mgoSession = mgoSession

	// create index that allow one unique magiccodes
	c := mgoSession.DB(bucketName).C(collectionName)
	index := mgo.Index{
		Key:    []string{"magiccode"},
		Unique: true,
	}
	err = c.EnsureIndex(index)
	if err != nil {
		return nil, err
	}

	return &newmm, nil
}

// StoreFileDetails stores the collected FileDetails in
// the MongoDB database.
func (mm *MongoDBManager) StoreFileDetails(fd *structs.FileDetails) error {
	c := mm.mgoSession.DB(bucketName).C(collectionName)
	err := c.Insert(fd)
	if err != nil {
		return err
	}

	return nil
}

// GetFiles return all stored FileDetails from MongoDB database.
// The result slice is sorted by data.
func (mm *MongoDBManager) GetFiles(files *structs.SortedFileDetails) error {
	c := mm.mgoSession.DB(bucketName).C(collectionName)

	err := c.Find(bson.M{}).All(files)
	if err != nil {
		return err
	}

	//FIXME: do I need to manuall sort or can I do this
	// during the MongoDB query
	sort.Sort(*files)

	return nil
}

// LoadDetailsFromMagiccode fetch FileDetails from MongoDB which are
// under the specified magic code.
func (mm *MongoDBManager) LoadDetailsFromMagiccode(magiccode string, fd *structs.FileDetails) error {
	c := mm.mgoSession.DB(bucketName).C(collectionName)

	err := c.Find(bson.M{"magiccode": magiccode}).One(fd)
	if err != nil {
		return err
	}

	return nil
}
