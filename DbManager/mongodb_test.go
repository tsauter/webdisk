package DbManager

import (
	"flag"
	"fmt"
	"gitlab.com/tsauter/webdisk/structs"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"reflect"
	"sort"
	"testing"
)

var (
	mongodbHostname = flag.String("mongodb-hostname", "localhost", "server name for mongodb tests")
	mgoTestSession  *mgo.Session
)

func init() {
	flag.Parse()

	// create a session to MongoDB to validate the mongo db backend
	var err error
	mgoTestSession, err = mgo.Dial(*mongodbHostname)
	if err != nil {
		panic(fmt.Errorf("failed to create MongoDB session: %s", err.Error()))
	}
	mgoTestSession.SetMode(mgo.Monotonic, true)
}

func TestStoreFileDetailsMongo(t *testing.T) {
	var m DbManager
	var err error
	if m, err = NewMongoDBManager(*mongodbHostname); err != nil {
		t.Fatalf("failed to initialize mongodb manager: %s", err.Error())
	}

	// open the MongoDB collection
	c := mgoTestSession.DB(bucketName).C(collectionName)

	// process every test case
	// wrap it in a funct to use defer
	for _, tc := range testcases {
		func() {
			// write data to mongodb
			if err = m.StoreFileDetails(&tc.fd); err != nil {
				t.Errorf("[%s] failed to store file details: %s", tc.casename, err.Error())
			}
			defer tearDownMongo(tc.fd.MagicCode)

			var fds []structs.FileDetails
			err = c.Find(bson.M{"magiccode": tc.fd.MagicCode}).All(&fds)
			if err != nil {
				t.Errorf("[%s] failed to fetch file details from mongodb: %s", tc.casename, err.Error())
			}
			// we expect only one entry
			if len(fds) < 1 {
				t.Errorf("no entry in mongodb found")
				return
			} else if len(fds) > 1 {
				t.Errorf("more than one entry found (%d elements)", len(fds))
				return
			}

			// compare the returnes fd entry from mongodb
			if !reflect.DeepEqual(tc.fd, fds[0]) {
				t.Errorf("[%s] retreived fd from mongodb is different", tc.casename)
				return
			}
		}()
	}
}

func TestLoadDetailsFromMagiccodeMongo(t *testing.T) {
	var m DbManager
	var err error
	if m, err = NewMongoDBManager(*mongodbHostname); err != nil {
		t.Fatalf("failed to initialize mongodb manager: %s", err.Error())
	}

	// open the MongoDB collection
	c := mgoTestSession.DB(bucketName).C(collectionName)

	// process every test case
	// wrap it in a funct to use defer
	for _, tc := range testcases {
		func() {
			// write data to mongodb
			if err = m.StoreFileDetails(&tc.fd); err != nil {
				t.Errorf("[%s] failed to store file details: %s", tc.casename, err.Error())
			}
			defer tearDownMongo(tc.fd.MagicCode)

			var fds []structs.FileDetails
			err = c.Find(bson.M{"magiccode": tc.fd.MagicCode}).All(&fds)
			if err != nil {
				t.Errorf("[%s] failed to fetch file details from mongodb: %s", tc.casename, err.Error())
			}
			// we expect only one entry
			if len(fds) < 1 {
				t.Errorf("no entry in mongodb found")
				return
			} else if len(fds) > 1 {
				t.Errorf("more than one entry found (%d elements)", len(fds))
				return
			}

			// compare the returnes fd entry from mongodb
			if !reflect.DeepEqual(tc.fd, fds[0]) {
				t.Errorf("[%s] retreived fd from mongodb is different", tc.casename)
				return
			}

			// load file details from json file and validate both stucts (equality)
			var fd structs.FileDetails
			err = m.LoadDetailsFromMagiccode(tc.fd.MagicCode, &fd)
			if err != nil {
				t.Errorf("[%s] failed to load details from magic code: %s", tc.casename, err.Error())
				return
			}
		}()
	}
}

func TestGetFilesMongo(t *testing.T) {
	var m DbManager
	var err error
	if m, err = NewMongoDBManager(*mongodbHostname); err != nil {
		t.Fatalf("failed to initialize mongodb manager: %s", err.Error())
	}

	// write json files from all test cases, after we have files to load
	// during GetFiles() function
	// additionally we store all processed fds in a sorted list
	var sortedCases structs.SortedFileDetails
	for _, tc := range testcases {
		// write data to mongodb
		if err = m.StoreFileDetails(&tc.fd); err != nil {
			t.Errorf("[%s] failed to store file details: %s", tc.casename, err.Error())
		}
		defer tearDownMongo(tc.fd.MagicCode)

		sortedCases = append(sortedCases, tc.fd)
	}
	sort.Sort(sortedCases)

	// read all available json files
	// (these files should be sorted in the same way we sorted above)
	var files structs.SortedFileDetails
	err = m.GetFiles(&files)
	if err != nil {
		t.Errorf("failed to get files: %s", err.Error())
		return
	}

	// make sure we have the same count of elements
	if len(files) != len(testcases) {
		t.Errorf("number of loaded files is different (loaded %d)", len(files))
		return
	}

	// loop over all testcases and verify that all loaded fds are correct
	// (loop of the sorted fds)
	for i, tcfd := range sortedCases {
		if !reflect.DeepEqual(tcfd, files[i]) {
			t.Errorf("loaded file details are different")
		}
	}
}

func tearDownMongo(magiccode string) error {
	c := mgoTestSession.DB(bucketName).C(collectionName)
	err := c.Remove(bson.M{"magiccode": magiccode})
	return err
}
