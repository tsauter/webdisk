package DbManager

import (
	"gitlab.com/tsauter/webdisk/structs"
)

// DbManager is the interface implemented by types that can store
// file details.
type DbManager interface {
	StoreFileDetails(fd *structs.FileDetails) error
	GetFiles(files *structs.SortedFileDetails) error
	LoadDetailsFromMagiccode(magiccode string, fd *structs.FileDetails) error
}
