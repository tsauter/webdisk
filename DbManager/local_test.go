package DbManager

import (
	"crypto/md5"
	"encoding/hex"
	"gitlab.com/tsauter/webdisk/structs"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"testing"
)

const (
	testStorageDir = "testoutput"
)

func TestStoreFileDetailsLocal(t *testing.T) {
	var m DbManager
	var err error
	if m, err = NewLocalDBManager(testStorageDir); err != nil {
		t.Fatalf("failed to initialize local db manager: %s", err.Error())
	}

	// process every test case
	// wrap it in a funct to use defer
	for _, tc := range testcases {
		func() {
			// write json file, and make sure the file gets removed on exit
			jsonfile := filepath.Join(testStorageDir, tc.fd.MagicCode+jsonExtension)
			if err = m.StoreFileDetails(&tc.fd); err != nil {
				t.Errorf("[%s] failed to store file details: %s", tc.casename, err.Error())
			}
			defer tearDownTestFile(jsonfile)

			// calculate checksum of generated json file, validate the checksum
			checksum, err := generateMd5(jsonfile)
			if err != nil {
				t.Errorf("[%s] failed to calulcate checksum: %s", tc.casename, err.Error())
				return
			}
			if checksum != tc.jsonMD5 {
				t.Errorf("[%s] test case has different checksum (file has %s)", tc.casename, checksum)
			}
		}()
	}
}

func TestLoadDetailsFromMagiccodeLocal(t *testing.T) {
	var m DbManager
	var err error
	if m, err = NewLocalDBManager(testStorageDir); err != nil {
		t.Fatalf("failed to initialize local db manager: %s", err.Error())
	}

	// process every test case
	// wrap it in a funct to use defer
	for _, tc := range testcases {
		func() {
			// write json file, and make sure the file gets removed on exit
			jsonfile := filepath.Join(testStorageDir, tc.fd.MagicCode+jsonExtension)
			if err = m.StoreFileDetails(&tc.fd); err != nil {
				t.Errorf("[%s] failed to store file details: %s", tc.casename, err.Error())
			}
			defer tearDownTestFile(jsonfile)

			// calculate checksum of generated json file, validate the checksum
			checksum, err := generateMd5(jsonfile)
			if err != nil {
				t.Errorf("[%s] failed to calulcate checksum: %s", tc.casename, err.Error())
				return
			}
			if checksum != tc.jsonMD5 {
				t.Errorf("[%s] test case has different checksum (file has %s)", tc.casename, checksum)
				return
			}

			// load file details from json file and validate both stucts (equality)
			var fd structs.FileDetails
			err = m.LoadDetailsFromMagiccode(tc.fd.MagicCode, &fd)
			if err != nil {
				t.Errorf("[%s] failed to load details from magic code: %s", tc.casename, err.Error())
				return
			}
			if !reflect.DeepEqual(tc.fd, fd) {
				t.Errorf("[%s] loaded file details are different", tc.casename)
			}
		}()
	}
}

func TestGetFilesLocal(t *testing.T) {
	var m DbManager
	var err error
	if m, err = NewLocalDBManager(testStorageDir); err != nil {
		t.Fatalf("failed to initialize local db manager: %s", err.Error())
	}

	// write json files from all test cases, after we have files to load
	// during GetFiles() function
	// additionally we store all processed fds in a sorted list
	var sortedCases structs.SortedFileDetails
	for _, tc := range testcases {
		jsonfile := filepath.Join(testStorageDir, tc.fd.MagicCode+jsonExtension)
		if err = m.StoreFileDetails(&tc.fd); err != nil {
			t.Errorf("[%s] failed to store file details: %s", tc.casename, err.Error())
			return
		}
		defer tearDownTestFile(jsonfile)

		sortedCases = append(sortedCases, tc.fd)
	}
	sort.Sort(sortedCases)

	// read all available json files
	// (these files should be sorted in the same way we sorted above)
	var files structs.SortedFileDetails
	err = m.GetFiles(&files)
	if err != nil {
		t.Errorf("failed to get files: %s", err.Error())
		return
	}

	// make sure we have the same count of elements
	if len(files) != len(testcases) {
		t.Errorf("number of loaded files is different (loaded %d)", len(files))
		return
	}

	// loop over all testcases and verify that all loaded fds are correct
	// (loop of the sorted fds)
	for i, tcfd := range sortedCases {
		if !reflect.DeepEqual(tcfd, files[i]) {
			t.Errorf("loaded file details are different")
		}
	}
}

func tearDownTestFile(filename string) error {
	return os.Remove(filename)
}

func generateMd5(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := md5.New()
	_, err = io.Copy(hash, file)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(hash.Sum(nil)), nil
}
