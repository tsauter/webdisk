package AuthManager

import (
	"path/filepath"
	"testing"
)

func TestTimeConsuming(t *testing.T) {
	var cases = []struct {
		name               string
		expectedTestResult bool
		username           string
		password           string
		isAdmin            bool
	}{
		{"1", true, "test", "test", false},
	}
	var am *AuthManager
	am, err := New(filepath.Join("fixtures", "users.json"))
	if err != nil {
		t.Errorf("initializing AuthManager failed: %s", err.Error())
		return
	}

	// walk over each case
	for _, testcase := range cases {
		result := am.VerifyUser(testcase.username, testcase.password)
		if (result == nil) != testcase.expectedTestResult {
			t.Errorf("test case %s failed: %s", testcase.name, result.Error())
		}
	}

}
