package AuthManager

import (
	"encoding/json"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"regexp"
	"strings"
	"sync"
)

type AuthManager struct {
	mu       sync.Mutex
	filename string
	Users    []User
}

type User struct {
	Username string `json:"Username"`
	Secret   []byte `json:"Secret"`
	IsAdmin  bool   `json:"IsAdmin"`
}

var (
	usernameRegex = regexp.MustCompile("^[A-Za-z][A-Za-z0-9]+$")
)

// New creates a new AuthManager.
func New(filename string) (*AuthManager, error) {
	var newam = AuthManager{filename: filename}

	if err := newam.LoadDatabase(); err != nil {
		return nil, err
	}

	return &newam, nil
}

func (am *AuthManager) LoadDatabase() error {
	file, err := ioutil.ReadFile(am.filename)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(file, &am.Users); err != nil {
		return err
	}

	return nil
}

func (am *AuthManager) SaveDatabase() error {
	b, err := json.Marshal(am.Users)
	if err != nil {
		return err
	}

	if err := ioutil.WriteFile(am.filename, b, 0600); err != nil {
		return err
	}

	return nil
}

func (am *AuthManager) VerifyUser(username string, password string) error {
	if !usernameRegex.MatchString(username) {
		return errors.New("invalid username")
	}

	for _, user := range am.Users {
		if user.Username == username {
			if len(user.Secret) == 0 {
				return errors.New("empty password")
			}

			err := bcrypt.CompareHashAndPassword(user.Secret, []byte(password))
			if err != nil {
				return errors.New("invalid password")
			}

			return nil
		}
	}

	return errors.New("user not found")
}

func (am *AuthManager) IsAdmin(username string) bool {
	for _, user := range am.Users {
		if (user.Username == username) && (user.IsAdmin) {
			return true
		}
	}

	return false
}

func (am *AuthManager) CreateUser(username string, password string, isAdmin bool) (*User, error) {
	if !usernameRegex.MatchString(username) {
		return nil, errors.New("invalid username")
	}

	secret, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	newuser := User{Username: strings.ToLower(username), Secret: secret, IsAdmin: isAdmin}

	for _, user := range am.Users {
		if user.Username == newuser.Username {
			return nil, errors.New("user already exist")
		}
	}

	am.Users = append(am.Users, newuser)

	if err := am.SaveDatabase(); err != nil {
		return nil, err
	}

	return &newuser, nil
}
