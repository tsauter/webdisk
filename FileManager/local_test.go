package FileManager

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/tsauter/webdisk/structs"
)

var (
	fixturesPath  = "fixtures"
	testfilesPath = "testfiles"

	fixtures = []struct {
		url      string
		filename string
		checksum string
	}{
		{
			"https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz",
			"go1.8.3.linux-amd64.tar.gz",
			"f412132cdce75d41e3bcb0d68a33fc66",
		},
		{
			"https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.11.6.tar.xz",
			"linux-4.11.6.tar.xz",
			"ed5930d543c077b82fa8a82fdbef1c52",
		},
		{
			"https://ftp.openbsd.org/pub/OpenBSD/patches/6.1/common/001_dhcpd.patch.sig",
			"001_dhcpd.patch.sig",
			"2cb1cbd2b7d4867de905d4ee5d103339",
		},
	}
)

func init() {
	downloadFixtures := flag.Bool("download-fixtures", false, "download files required for fixture tests")
	flag.Parse()

	// Download all required fixture files and store them into
	// the fixtures/ folder.
	// These files will be used in lates tests.
	if *downloadFixtures {
		fmt.Printf("Downloading all fixture files...\n")

		for _, fixfile := range fixtures {
			targetfile := filepath.Join(fixturesPath, fixfile.filename)
			err := downloadHelper(fixfile.url, targetfile)
			if err != nil {
				panic(err)
			}

			c, err := generateMD5(targetfile)
			if err != nil {
				panic(err)
			}

			if c != fixfile.checksum {
				panic(fmt.Errorf("checksum missmatch"))
			}
		}
	}
}

func TestPingLocal(t *testing.T) {
	t.Skip("no test for ping")
}

func TestGetStorageDirLocal(t *testing.T) {
	storageDir, err := filepath.Abs(testfilesPath)
	if err != nil {
		t.Fatalf("unable to convert directory: %s", err.Error())
	}

	dbManager := &DbManagerMock{}
	filer, err := NewLocalManager(storageDir, dbManager)
	if err != nil {
		t.Fatalf("failed to initialize local file manager: %s", err.Error())
	}

	dir := filer.GetStorageDir()
	if dir != storageDir {
		t.Errorf("different storage dir reported: %s, %s", storageDir, dir)
	}
}

func TestStoreUploadLocal(t *testing.T) {
	storageDir, err := filepath.Abs(testfilesPath)
	if err != nil {
		t.Fatalf("unable to convert directory: %s", err.Error())
	}

	dbManager := &DbManagerMock{}
	dbManager.Entries = make(map[string]structs.FileDetails)

	filer, err := NewLocalManager(storageDir, dbManager)
	if err != nil {
		t.Fatalf("failed to initialize local file manager: %s", err.Error())
	}

	for _, fixfile := range fixtures {
		sourcefile := filepath.Join(fixturesPath, fixfile.filename)
		var expiryDays uint = 31
		username := "n/a"
		clientIP := "1.1.1.1"

		r, err := os.Open(sourcefile)
		if err != nil {
			t.Fatalf("failed to open source file: %s", err.Error())
		}

		fd, err := filer.StoreUpload(sourcefile, expiryDays, username, clientIP, r)
		if err != nil {
			t.Errorf("failed to upload source file: %s", err.Error())
		}

		targetfile := filepath.Join(storageDir, fd.MagicCode)
		defer tearDownTestFile(targetfile)

		checksum, err := generateMD5(targetfile)
		if err != nil {
			t.Errorf("failed to generate checksum for targetfile: %s", err.Error())
		}

		// validate checksum of both files
		if checksum != fixfile.checksum {
			t.Errorf("Uploaded file is different (checksum mistmatch)")
		}
	}
}

func populateStorage() (FileManager, int, error) {
	storageDir, err := filepath.Abs(testfilesPath)
	if err != nil {
		return nil, 0, fmt.Errorf("unable to convert directory: %s", err.Error())
	}

	dbManager := &DbManagerMock{}
	dbManager.Entries = make(map[string]structs.FileDetails)

	filer, err := NewLocalManager(storageDir, dbManager)
	if err != nil {
		return nil, 0, fmt.Errorf("failed to initialize local file manager: %s", err.Error())
	}

	processedFiles := 0

	for _, fixfile := range fixtures {
		sourcefile := filepath.Join(fixturesPath, fixfile.filename)
		var expiryDays uint = 31
		username := "n/a"
		clientIP := "1.1.1.1"

		r, err := os.Open(sourcefile)
		if err != nil {
			return nil, 0, fmt.Errorf("failed to open source file: %s", err.Error())
		}

		fd, err := filer.StoreUpload(sourcefile, expiryDays, username, clientIP, r)
		if err != nil {
			return nil, 0, fmt.Errorf("failed to upload source file: %s", err.Error())
		}

		targetfile := filepath.Join(storageDir, fd.MagicCode)

		checksum, err := generateMD5(targetfile)
		if err != nil {
			return nil, 0, fmt.Errorf("failed to generate checksum for targetfile: %s", err.Error())
		}

		// validate checksum of both files
		if checksum != fixfile.checksum {
			return nil, 0, fmt.Errorf("Uploaded file is different (checksum mistmatch) %s", checksum)
		}

		processedFiles++
	}

	return filer, processedFiles, nil
}

func xTestLoadDetailsFromMagiccodeLocal(t *testing.T) {
	// load test files in the storage
	filer, numFiles, err := populateStorage()
	if err != nil {
		t.Errorf("Populating failed: %s", err.Error())
	}

	dir, err := os.Open(testfilesPath)
	if err != nil {
		t.Fatalf("failed to read fixtures directory: %s", err.Error())
	}
	defer dir.Close()

	magicfiles, err := dir.Readdir(-1)
	if err != nil {
		t.Fatalf("failed to read fixtures directory: %s", err.Error())
	}

	processedFiles := 0

	for _, fi := range magicfiles {
		magiccode := fi.Name()
		targetfile := filepath.Join(testfilesPath, magiccode)
		defer tearDownTestFile(targetfile)

		var fd structs.FileDetails
		err = filer.LoadDetailsFromMagiccode(magiccode, &fd)
		if err != nil {
			t.Errorf("failed to load file from magiccode: %s (%s)", err.Error(), targetfile)
		}

		processedFiles++
	}

	if processedFiles != numFiles {
		t.Fatalf("not enough files process")
	}
}

func TestStreamFileLocal(t *testing.T) {
	// load test files in the storage
	storageDir, err := filepath.Abs(testfilesPath)
	if err != nil {
		t.Fatalf("unable to generare absolute path: %s", err.Error())
	}

	dbManager := &DbManagerMock{}
	dbManager.Entries = make(map[string]structs.FileDetails)

	filer, err := NewLocalManager(storageDir, dbManager)
	if err != nil {
		t.Fatalf("unable to create LocalManager: %s", err.Error())
	}

	numFiles := 0

	for _, fixfile := range fixtures {
		sourcefile := filepath.Join(fixturesPath, fixfile.filename)
		var expiryDays uint = 31
		username := "n/a"
		clientIP := "1.1.1.1"

		r, err := os.Open(sourcefile)
		if err != nil {
			t.Fatalf("failed to open sourcefile: %s", err.Error())
		}

		fd, err := filer.StoreUpload(sourcefile, expiryDays, username, clientIP, r)
		if err != nil {
			t.Fatalf("failed to upload file: %s", err.Error())
		}

		targetfile := filepath.Join(storageDir, fd.MagicCode)

		checksum, err := generateMD5(targetfile)
		if err != nil {
			t.Fatalf("failed to generate checksum: %s", err.Error())
		}

		// validate checksum of both files
		if checksum != fixfile.checksum {
			t.Fatalf("checksum is different")
		}

		numFiles++
	}

	dir, err := os.Open(testfilesPath)
	if err != nil {
		t.Fatalf("failed to read fixtures directory: %s", err.Error())
	}
	defer dir.Close()

	magicfiles, err := dir.Readdir(-1)
	if err != nil {
		t.Fatalf("failed to read fixtures directory: %s", err.Error())
	}

	processedFiles := 0

	for _, fi := range magicfiles {
		magiccode := fi.Name()
		targetfile := filepath.Join(testfilesPath, magiccode)
		defer tearDownTestFile(targetfile)

		var fd structs.FileDetails
		err = filer.LoadDetailsFromMagiccode(magiccode, &fd)
		if err != nil {
			t.Fatalf("failed to load file from magiccode: %s (%s)", err.Error(), targetfile)
		}

		w := httptest.NewRecorder()

		err = filer.StreamFile(&fd, w)
		if err != nil {
			t.Fatalf("failed to stream file from magiccode: %s (%s)", err.Error(), targetfile)
		}

		resp := w.Result()
		defer resp.Body.Close()

		f, err := ioutil.TempFile(testfilesPath, "test___")
		if err != nil {
			t.Fatalf("failed to create temporary file: %s\n", err.Error())
		}
		defer f.Close()
		defer os.Remove(f.Name())

		io.Copy(f, resp.Body)
		if err := f.Close(); err != nil {
			t.Fatalf("failed to close temporary file: %s: %s\n", f.Name(), err.Error())
		}

		checksum, err := generateSHA256(f.Name())
		if err != nil {
			t.Errorf("failed to generate checksum for targetfile: %s", err.Error())
		}

		// validate checksum of both files
		if checksum != fd.Checksum {
			t.Errorf("Uploaded file is different (checksum mistmatch)")
		}

		processedFiles++
	}

	if processedFiles != numFiles {
		t.Fatalf("not enough files process")
	}
}

type DbManagerMock struct {
	Entries map[string]structs.FileDetails
}

func (m *DbManagerMock) StoreFileDetails(fd *structs.FileDetails) error {
	m.Entries[fd.MagicCode] = *fd
	return nil
}

func (m *DbManagerMock) GetFiles(files *structs.SortedFileDetails) error {
	return nil
}

func (m *DbManagerMock) LoadDetailsFromMagiccode(magiccode string, fd *structs.FileDetails) error {
	tmp, ok := m.Entries[magiccode]
	if ok != true {
		return fmt.Errorf("magiccode not found")
	}
	*fd = tmp

	return nil
}

func downloadHelper(url, targetfile string) error {
	out, err := os.Create(targetfile)
	if err != nil {
		return err
	}
	defer out.Close()

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	err = out.Close()
	if err != nil {
		return err
	}

	return nil
}

func tearDownTestFile(filename string) error {
	return os.Remove(filename)
}

func generateMD5(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := md5.New()
	_, err = io.Copy(hash, file)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(hash.Sum(nil)), nil
}

func generateSHA256(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := sha256.New()
	_, err = io.Copy(hash, file)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(hash.Sum(nil)), nil
}
