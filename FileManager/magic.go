package FileManager

import (
	"math/rand"
	"regexp"
	"sync"
	"time"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var magicCodeRegex = regexp.MustCompile("^[A-Za-z0-9]+$")

var src = rand.NewSource(time.Now().UnixNano())

// NewMagicCode generates the magic code
func NewMagicCode(n int) string {
	var mutex sync.Mutex
	mutex.Lock()

	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	mutex.Unlock()

	return string(b)
}

// ValidateMagicCode validates the magic code from the user
// against a whitelist of valid characters
func ValidateMagicCode(code string) bool {
	return magicCodeRegex.MatchString(code)
}
