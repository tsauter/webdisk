package FileManager

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/minio/minio-go"
	"gitlab.com/tsauter/webdisk/DbManager"
	"gitlab.com/tsauter/webdisk/structs"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	BucketName   = "webdisk"
	LocationName = "de-1"
)

// make sure FileManager is always satisfied
var _ FileManager = &MinioManager{}

type MinioManager struct {
	endpoint    string
	dbManager   DbManager.DbManager
	minioClient *minio.Client
	magicLength int
}

type MinioConfig struct {
	Endpoint  string `yaml:"Endpoint"`
	AccessKey string `yaml:"AccessKey"`
	SecretKey string `yaml:"SecretKey"`
	UseTLS    bool   `yaml:"TLS"`
}

type MongoDBConfig struct {
	Servers []string `yaml:"Servers"`
}

// New creates a new FileManager.
func NewMinioManager(minioEndpoint string, minioAccessID string, minioSecretKey string, minioUseTLS bool, dbManager DbManager.DbManager) (*MinioManager, error) {
	var newmm = MinioManager{magicLength: MagicCodeLength, dbManager: dbManager}

	newmm.endpoint = minioEndpoint
	minioClient, err := minio.New(minioEndpoint, minioAccessID, minioSecretKey, minioUseTLS)
	if err != nil {
		return nil, err
	}
	newmm.minioClient = minioClient

	err = minioClient.MakeBucket(BucketName, LocationName)
	if err != nil {
		exists, err := minioClient.BucketExists(BucketName)
		if err != nil || !exists {
			return nil, err
		}
	}

	return &newmm, nil
}

// Ping makes a quick health check
func (mm *MinioManager) Ping() error {
	return nil
}

func (mm *MinioManager) GetStorageDir() string {
	return fmt.Sprintf("%#v", mm.endpoint)
}

// StorageUpload takes the input stream from the http upload and create both
// files. The uploaded file and the corresponding JSON file.
func (mm *MinioManager) StoreUpload(realnameFilename string, expiryDays uint, username string, clientIp string, r io.Reader) (*structs.FileDetails, error) {
	// Request a new magic code from the storage backend and build the full filename
	magiccode := NewMagicCode(15)

	hasher := sha256.New()
	tr := io.TeeReader(r, hasher)

	_, err := mm.minioClient.PutObject(BucketName, magiccode, tr, "application/octet-stream")
	if err != nil {
		return nil, err
	}

	maxFileDuration := time.Hour * (24 * time.Duration(expiryDays))

	fd := structs.FileDetails{
		MagicCode:       magiccode,
		ExpiryTimestamp: time.Now().UTC().Add(maxFileDuration),
		UploadFilename:  realnameFilename,
		UploadTimestamp: time.Now().UTC(),
		Uploader:        username,
		UploaderIP:      clientIp,
		Checksum:        strings.ToLower(hex.EncodeToString(hasher.Sum(nil))),
	}

	fileinfo, err := mm.minioClient.StatObject(BucketName, fd.MagicCode)
	if err != nil {
		return &fd, err
	}
	fd.Size = fileinfo.Size

	if err = mm.dbManager.StoreFileDetails(&fd); err != nil {
		return &fd, err
	}

	return &fd, nil
}

// StorageUpload takes the input stream from the http upload and create both
// files. The uploaded file and the corresponding JSON file.
func (mm *MinioManager) StreamFile(fd *structs.FileDetails, w http.ResponseWriter) error {
	object, err := mm.minioClient.GetObject(BucketName, fd.MagicCode)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Length", strconv.FormatInt(fd.Size, 10))
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", fd.UploadFilename))

	_, err = io.Copy(w, object)
	if err != nil {
		return err
	}

	return nil
}

// GetFiles reads all available json files from the storage directory
func (mm *MinioManager) GetFiles(files *structs.SortedFileDetails) error {
	return mm.dbManager.GetFiles(files)
}

// LoadDetailsMagiccode read the corresponding json file in a FileDetails struct
func (mm *MinioManager) LoadDetailsFromMagiccode(magiccode string, fd *structs.FileDetails) error {
	err := mm.dbManager.LoadDetailsFromMagiccode(magiccode, fd)
	if fd.MagicCode == "" {
		return fmt.Errorf("magiccode is invalid")
	}
	return err
}

//
func (mm *MinioManager) DeleteExpiredFiles() (int, error) {
	//c := mm.mgoSession.DB(BucketName).C(CollectionName)

	var files structs.SortedFileDetails
	mm.GetFiles(&files)

	currentTimeUtc := time.Now().UTC()
	//fmt.Printf("UTC now: %s\n", currentTimeUtc)

	countProcessed := 0
	var last_error error

	for _, fd := range files {
		//logger.Printf("%#v", fd)
		fmt.Printf("File:      %s\n", fd.UploadFilename)
		//fmt.Printf("Timestamp: %s\n", fd.ExpiryTimestamp)
		//fmt.Printf("Now      : %s\n", currentTimeUtc)

		if fd.ExpiryTimestamp.Before(currentTimeUtc) {
			fmt.Printf("Delete expired file: %#v\n", fd)

			//err := c.Remove(bson.M{"magiccode": fd.MagicCode})
			//if err != nil {
			//last_error = err
			//continue
			//}

			err := mm.minioClient.RemoveObject(BucketName, fd.MagicCode)
			if err != nil {
				last_error = err
				continue
			}

			countProcessed += 1
		}

	}

	if last_error != nil {
		return countProcessed, last_error
	}

	return countProcessed, nil
}
