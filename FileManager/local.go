package FileManager

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/tsauter/webdisk/DbManager"
	"gitlab.com/tsauter/webdisk/structs"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

// make sure FileManager is always satisfied
var _ FileManager = &LocalManager{}

type LocalManager struct {
	mu          sync.Mutex
	dbManager   DbManager.DbManager
	storageDir  string
	magicLength int
}

type LocalStorageConfig struct {
	Directory string `yaml:"Directory"`
}

// New creates a new FileManager.
func NewLocalManager(dir string, dbManager DbManager.DbManager) (*LocalManager, error) {
	var newfm = LocalManager{magicLength: MagicCodeLength, dbManager: dbManager}

	if err := newfm.SetStorageDir(dir); err != nil {
		return nil, err
	}

	return &newfm, nil
}

// SetStorageDir set the master directory for all files
func (lm *LocalManager) SetStorageDir(dir string) error {
	lm.mu.Lock()
	defer lm.mu.Unlock()

	absDir, err := filepath.Abs(dir)
	if err != nil {
		return err
	}
	lm.storageDir = absDir

	return nil
}

// GetStorageDir returns the real path
// where the local files are stored
func (lm *LocalManager) GetStorageDir() string {
	return lm.storageDir
}

// Ping makes a quick health check
func (lm *LocalManager) Ping() error {
	dir, err := filepath.Abs(lm.storageDir)
	if err != nil {
		return err
	}

	if _, err := os.Stat(dir); err != nil {
		return err
	}

	return nil
}

// StorageUpload takes the input stream from the http upload and create both
// files. The uploaded file and the corresponding JSON file.
func (lm *LocalManager) StoreUpload(realnameFilename string, expiryDays uint, username string, clientIp string, r io.Reader) (*structs.FileDetails, error) {
	// Request a new magic code from the storage backend and build the full filename
	magiccode := NewMagicCode(15)
	targetFilename := lm.BuildFilenameFromMagic(magiccode)

	f, err := os.OpenFile(targetFilename, os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	hasher := sha256.New()
	tr := io.TeeReader(r, hasher)

	_, err = io.Copy(f, tr)
	if err != nil {
		return nil, err
	}
	f.Close() // make sure the file is closed before we create the checksum

	maxFileDuration := time.Hour * (24 * time.Duration(expiryDays))

	fd := structs.FileDetails{
		MagicCode:       magiccode,
		ExpiryTimestamp: time.Now().UTC().Add(maxFileDuration),
		UploadFilename:  realnameFilename,
		UploadTimestamp: time.Now().UTC(),
		Uploader:        username,
		UploaderIP:      clientIp,
		Checksum:        strings.ToLower(hex.EncodeToString(hasher.Sum(nil))),
	}

	// Get file size
	fileinfo, err := os.Stat(targetFilename)
	if err != nil {
		return &fd, err
	}
	fd.Size = fileinfo.Size()

	if err = lm.dbManager.StoreFileDetails(&fd); err != nil {
		return &fd, err
	}

	return &fd, nil
}

// StorageUpload takes the input stream from the http upload and create both
// files. The uploaded file and the corresponding JSON file.
func (lm *LocalManager) StreamFile(fd *structs.FileDetails, w http.ResponseWriter) error {
	filename := lm.BuildFilenameFromMagic(fd.MagicCode)

	// make sure the returned filename points to an existing file
	fi, err := os.Stat(filename)
	if err != nil {
		return errors.Wrap(err, "failed to stat file")
	}
	switch mode := fi.Mode(); {
	case mode.IsRegular():
	default:
		return fmt.Errorf("resolved file does not exist: %s", filename)
	}

	realFile, err := os.Open(filename)
	if err != nil {
		return errors.Wrap(err, "failed to open file")
	}
	defer realFile.Close()

	w.Header().Set("Content-Length", strconv.FormatInt(fd.Size, 10))
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", fd.UploadFilename))

	_, err = io.Copy(w, realFile)
	if err != nil {
		return errors.Wrap(err, "failed to copy bytes")
	}

	return nil
}

// GetFiles reads all available json files from the storage directory
func (lm *LocalManager) GetFiles(files *structs.SortedFileDetails) error {
	return lm.dbManager.GetFiles(files)
}

// LoadDetailsMagiccode read the corresponding json file in a FileDetails struct
func (lm *LocalManager) LoadDetailsFromMagiccode(magiccode string, fd *structs.FileDetails) error {
	err := lm.dbManager.LoadDetailsFromMagiccode(magiccode, fd)
	if fd.MagicCode == "" {
		return fmt.Errorf("magiccode is invalid")
	}
	return err
}

//
func (lm *LocalManager) DeleteExpiredFiles() (int, error) {
	var files structs.SortedFileDetails
	lm.GetFiles(&files)

	currentTimeUtc := time.Now().UTC()
	//fmt.Printf("UTC now: %s\n", currentTimeUtc)

	countProcessed := 0
	var last_error error

	for _, fd := range files {
		//logger.Printf("%#v", fd)
		//fmt.Printf("File:      %s\n", fd.UploadFilename)
		//fmt.Printf("Timestamp: %s\n", fd.ExpiryTimestamp)
		//fmt.Printf("Now      : %s\n", currentTimeUtc)

		if fd.ExpiryTimestamp.Before(currentTimeUtc) {
			fmt.Printf("Delete expired file: %#v\n", fd)

			filename := lm.BuildFilenameFromMagic(fd.MagicCode)

			if err := os.Remove(filename); err != nil {
				last_error = err
			}
			//if err := os.Remove(filename + JsonExtension); err != nil {
			//last_error = err
			//}

			countProcessed += 1
		}

	}

	if last_error != nil {
		return countProcessed, last_error
	}

	return countProcessed, nil
}

// BuildFilenameFromMagic builds a full qualified filename for the give magiccode.
// This contains the uploaded file, not the corresponding JSON file
func (lm *LocalManager) BuildFilenameFromMagic(magiccode string) string {
	return filepath.Join(lm.storageDir, magiccode)
}
