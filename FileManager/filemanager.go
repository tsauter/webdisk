package FileManager

import (
	"io"
	"net/http"

	"gitlab.com/tsauter/webdisk/structs"
)

const (
	MagicCodeLength = 20
)

type FileManager interface {
	Ping() error
	GetStorageDir() string
	LoadDetailsFromMagiccode(magiccode string, fd *structs.FileDetails) error
	StreamFile(fd *structs.FileDetails, w http.ResponseWriter) error
	StoreUpload(realnameFilename string, expiryDays uint, username string, clientIp string, r io.Reader) (*structs.FileDetails, error)
	GetFiles(files *structs.SortedFileDetails) error
}
