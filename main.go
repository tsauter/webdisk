package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/cloudfoundry/bytefmt"
	"github.com/codegangsta/negroni"
	kitlog "github.com/go-kit/kit/log"
	"github.com/gorilla/csrf"
	gmux "github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/yosssi/ace"
	"gitlab.com/tsauter/webdisk/AuthManager"
	"gitlab.com/tsauter/webdisk/DbManager"
	"gitlab.com/tsauter/webdisk/FileManager"
	"gitlab.com/tsauter/webdisk/structs"
	"gopkg.in/yaml.v2"
	"html"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	sessionName = "webdisk"
)

func verifyFileStorage(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if err := filer.Ping(); err != nil {
		kitlogger.Log("client", detectRemoteIP(r), "storage", false, "error", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//kitlogger.Log("client", detectRemoteIp(r), "storage", true)

	next(w, r)
}

func getStringFromSession(r *http.Request, key string) string {
	session, err := sessionStore.Get(r, sessionName)
	if err != nil {
		return ""
	}

	val, ok := session.Values[key]
	if !ok || val == nil {
		return ""
	}

	return val.(string)
}

func verifyUser(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if (r.URL.Path == "/login") ||
		strings.HasPrefix(r.URL.Path, "/download") ||
		(r.URL.Path == "/") ||
		strings.HasPrefix(r.URL.Path, "/site_media") {

		next(w, r)
		return
	}

	if username := getStringFromSession(r, "User"); username != "" {
		if username != "" {
			next(w, r)
			return
		}
	}

	http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
}

// LoginPage struct passed during HTML rendering
type LoginPage struct {
	CsrfField    template.HTML
	ErrorMessage string
}

// DownloadPage struct passed during HTML rendering
type DownloadPage struct {
	CsrfField    template.HTML
	MagicCode    string
	FqdnWebRoot  string
	File         structs.FileDetails
	ErrorMessage string
}

// UploadPage struct passed during HTML rendering
type UploadPage struct {
	CsrfField    template.HTML
	FileFields   []string
	Files        []structs.FileDetails
	FqdnWebRoot  string
	ErrorMessage string
}

// ContentPage struct passed during HTML rendering
type ContentPage struct {
	CsrfField   template.HTML
	FqdnWebRoot string
	Files       []structs.FileDetails
}

// Config struct holds all configuration variabled
type Config struct {
	// //Common properties
	ListenAddress           string `yaml:"ListenAddress"`
	ListenAddressPrometheus string `yaml:"ListenAddressPrometheus"`
	ApplicationURL          string `yaml:"ApplicationUrl"`
	// CSRF/session protection
	CsrfToken    string `yaml:"CsrfToken"`
	CsrfDisabled bool   `yaml:"CsrfDisabled"`
	CsrfInsecure bool   `yaml:"CsrfInsecure"`
	SessionToken string `yaml:"SessionToken"`
	UserdbFile   string `yaml:"UserdbFile"`
	// Storage configuration
	ExpiryDays           uint                           `yaml:"ExpiryDays"`
	MaxUploadsPerRequest int                            `yaml:"MaxUploadsPerRequest"`
	DatabaseBackend      string                         `yaml:"DatabaseBackend"`
	StorageBackend       string                         `yaml:"StorageBackend"`
	Minio                FileManager.MinioConfig        `yaml:"Minio"`
	LocalStorage         FileManager.LocalStorageConfig `yaml:"LocalStorage"`
	MongoDB              FileManager.MongoDBConfig      `yaml:"MongoDB"`
}

var (
	cfg Config

	kitlogger = kitlog.NewLogfmtLogger(os.Stdout)
	filer     FileManager.FileManager

	sessionStore *sessions.CookieStore

	metricsTotalDownloads = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "webdisk",
		Name:      "downloads_total",
		Help:      "Total number of downloads.",
	})
	metricsTotalUploads = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "webdisk",
		Name:      "uploads_total",
		Help:      "Total number of uploads.",
	})

	version = "master"
)

func main() {
	flag.Parse()

	// load configuration settings
	cfgfile, err := ioutil.ReadFile("webdisk.yml")
	if err != nil {
		if !os.IsNotExist(err) {
			kitlogger.Log("webdisk", false, "error", "load configuration failed", "extra", err)
			os.Exit(1)
		}
	}

	err = yaml.Unmarshal(cfgfile, &cfg)
	if err != nil {
		kitlogger.Log("webdisk", false, "error", "invalid configuration file", "extra", err)
		os.Exit(1)
	}

	// validate the app-url
	if cfg.ApplicationURL != "" {
		if !govalidator.IsURL(cfg.ApplicationURL) {
			fmt.Printf("Not a valid application url specified: %s\n", cfg.ApplicationURL)
			os.Exit(1)
		}
	}

	// if csrf is not disabled, check for a valid csrf token
	if !cfg.CsrfDisabled {
		if len(cfg.CsrfToken) < 10 {
			kitlogger.Log("webdisk", false, "error", "invalid csrf token", "extra", cfg.CsrfToken)
			os.Exit(1)
		}
	}

	// validate session token
	if len(cfg.SessionToken) < 10 {
		kitlogger.Log("webdisk", false, "error", "invalid session token", "extra", cfg.SessionToken)
		os.Exit(1)
	}
	sessionStore = sessions.NewCookieStore([]byte(cfg.SessionToken))

	// load auth database
	authMgr, err := AuthManager.New(cfg.UserdbFile)
	if err != nil {
		kitlogger.Log("authmanager", false, "error", "initializing AuthManager failed", "extra", err)
		os.Exit(1)
	}

	// load and initialize db backend
	var dbManager DbManager.DbManager
	switch strings.ToLower(cfg.DatabaseBackend) {
	case "local":
		if cfg.LocalStorage.Directory == "" {
			cfg.LocalStorage.Directory = "./files"
		}
		dbManager, err = DbManager.NewLocalDBManager(cfg.LocalStorage.Directory)
	case "mongodb":
		mgoServers := strings.Join(cfg.MongoDB.Servers, ",")
		dbManager, err = DbManager.NewMongoDBManager(mgoServers)
	default:
		err = fmt.Errorf("unsupported database backend")
	}
	if err != nil {
		kitlogger.Log("dbmanager", false, "error", "initializing DbManager failed", "extra", err)
		os.Exit(1)
	}

	// load and initialize storage backend
	switch strings.ToLower(cfg.StorageBackend) {
	case "local":
		if cfg.LocalStorage.Directory == "" {
			cfg.LocalStorage.Directory = "./files"
		}
		filer, err = FileManager.NewLocalManager(cfg.LocalStorage.Directory, dbManager)
	case "minio":
		filer, err = FileManager.NewMinioManager(
			cfg.Minio.Endpoint,
			cfg.Minio.AccessKey,
			cfg.Minio.SecretKey,
			cfg.Minio.UseTLS,
			dbManager)
	default:
		err = fmt.Errorf("unsupported storage backend")
	}
	if err != nil {
		kitlogger.Log("storage", false, "error", "initializing FileManager failed", "extra", err)
		os.Exit(1)
	}
	kitlogger.Log("storage", true, "type", cfg.StorageBackend, "path", filer.GetStorageDir())

	mux := gmux.NewRouter()

	funcMap := template.FuncMap{
		"Size": func(values ...interface{}) string {
			i := values[0].(int64)
			return bytefmt.ByteSize(uint64(i))
		},
		"Date": func(values ...interface{}) string {
			t := values[0].(time.Time)
			return t.Format(time.RFC1123)
		},
	}
	aceOpts := ace.Options{FuncMap: funcMap}

	// URL: /
	// Redirect all requests to /download
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/download", http.StatusFound)
		return
	}).Methods("GET")

	// URL: /download
	// Display the default download page without any magic code
	mux.HandleFunc("/download", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := ace.Load("templates/download", "", &aceOpts)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		p := DownloadPage{}
		p.CsrfField = csrf.TemplateField(r)
		p.FqdnWebRoot = getDownloadURL(r)

		if err = tmpl.Execute(w, p); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}).Methods("GET")

	// URL: /download/<MAGICCODE>?start=1
	// Immediately start download the file (start=1)
	mux.HandleFunc("/download/{magiccode:[A-Za-z0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		magiccode, _ := gmux.Vars(r)["magiccode"]
		if !FileManager.ValidateMagicCode(magiccode) {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "invalid magic code", "magiccode", magiccode)
			http.Error(w, "Invalid magic code", http.StatusInternalServerError)
			return
		}

		tmpl, err := ace.Load("templates/download", "", &aceOpts)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		p := DownloadPage{}
		p.CsrfField = csrf.TemplateField(r)
		p.FqdnWebRoot = getDownloadURL(r)

		var fd structs.FileDetails
		err = filer.LoadDetailsFromMagiccode(magiccode, &fd)
		if err != nil {
			// The magic code is invalid or was not found, return 500 and exit
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "invalid magic code requested", "magiccode", magiccode)
			p.ErrorMessage = fmt.Sprintf("Invalid magic code: %s",
				html.EscapeString(magiccode))
			p.MagicCode = ""

			if err = tmpl.Execute(w, p); err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}

		p.MagicCode = fd.MagicCode
		p.File = fd

		if err := filer.StreamFile(&fd, w); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "original file not accessible", "magiccode", magiccode, "extra", err)
			p.ErrorMessage = fmt.Sprintf("File temporarily not available.")
			p.MagicCode = magiccode

			if err = tmpl.Execute(w, p); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			return
		}

		// increment the internal counter for downloads
		metricsTotalDownloads.Inc()
		kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "download", true, "magiccode", magiccode)

	}).Methods("GET").Queries("start", "1")

	// URL: /download/<MAGICCODE>
	// Display details about the file, and allow downloading them
	mux.HandleFunc("/download/{magiccode:[A-Za-z0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		magiccode, _ := gmux.Vars(r)["magiccode"]
		if !FileManager.ValidateMagicCode(magiccode) {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "invalid magic code", "extra", magiccode)
			http.Error(w, "Invalid magic code", http.StatusInternalServerError)
			return
		}

		tmpl, err := ace.Load("templates/download", "", &aceOpts)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		p := DownloadPage{}
		p.CsrfField = csrf.TemplateField(r)
		p.FqdnWebRoot = getDownloadURL(r)

		var fd structs.FileDetails
		err = filer.LoadDetailsFromMagiccode(magiccode, &fd)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load file details", "magiccode", magiccode, "extra", err)
			p.ErrorMessage = fmt.Sprintf("Invalid magic code: %s",
				html.EscapeString(magiccode))
			p.MagicCode = ""
		} else {
			p.MagicCode = fd.MagicCode
			p.File = fd
		}

		if err = tmpl.Execute(w, p); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "download", false, "magiccode", magiccode)
	}).Methods("GET")

	// URL: POST:/download/<MAGICCODE>
	// Return JSON encoded details about the file, in case of an error 404 is returned
	mux.HandleFunc("/download", func(w http.ResponseWriter, r *http.Request) {
		magiccode := r.FormValue("magiccode")
		if !FileManager.ValidateMagicCode(magiccode) {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "invalid magic code", "extra", magiccode)
			http.Error(w, "Invalid magic code", http.StatusNotFound)
			return
		}

		var fd structs.FileDetails
		err := filer.LoadDetailsFromMagiccode(magiccode, &fd)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load file details", "magiccode", magiccode, "extra", err)
			errorMessage := fmt.Sprintf("Invalid magic code: %s",
				html.EscapeString(magiccode))
			http.Error(w, errorMessage, http.StatusNotFound)
			return
		}

		if err := json.NewEncoder(w).Encode(fd); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "json encoding failed", "magiccode", magiccode, "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "download", false, "magiccode", magiccode)
	}).Methods("POST")

	mux.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		var tmpl *template.Template
		var err error

		p := UploadPage{}
		p.CsrfField = csrf.TemplateField(r)
		p.FqdnWebRoot = getDownloadURL(r)
		// Build a list of valid upload fields
		for i := 1; i < cfg.MaxUploadsPerRequest+1; i++ {
			p.FileFields = append(p.FileFields, fmt.Sprintf("uploadedfile%d", i))
		}

		// Maybe this is an redirect from an earlier POST request.
		// In this case the session variable UploadedFile is populated with a
		// magic code.
		// To avoid looping, this session variable gets removed immediately.
		var uploadedMagiccodes []string
		session, err := sessionStore.Get(r, sessionName)
		if err == nil {
			val, ok := session.Values["UploadedFiles"]
			if ok {
				uploadedMagiccodes = val.([]string)
			}
		}
		//kitlogger.Log("client", detectRemoteIp(r), "request", r.URL, "debug", "files in session", "extra", fmt.Sprintf("%#v", uploaded_magiccodes))

		if len(uploadedMagiccodes) > 0 {
			session, err := sessionStore.Get(r, sessionName)
			if err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to get session", "name", sessionName, "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			delete(session.Values, "UploadedFiles")
			if err := session.Save(r, w); err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to save session", "name", sessionName, "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			for _, magiccode := range uploadedMagiccodes {
				var fd structs.FileDetails
				if err = filer.LoadDetailsFromMagiccode(magiccode, &fd); err != nil {
					kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load file details", "magiccode", magiccode, "extra", err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				p.Files = append(p.Files, fd)
			}

			tmpl, err = ace.Load("templates/upload_success", "", &aceOpts)
			if err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

		} else {
			tmpl, err = ace.Load("templates/upload", "", &aceOpts)
			if err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		if err = tmpl.Execute(w, p); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}).Methods("GET")

	mux.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := ace.Load("templates/upload", "", &aceOpts)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		session, err := sessionStore.Get(r, sessionName)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to get session", "name", sessionName, "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		p := UploadPage{}
		p.CsrfField = csrf.TemplateField(r)
		p.FqdnWebRoot = getDownloadURL(r)
		// Build a list of valid upload fields
		for i := 1; i < cfg.MaxUploadsPerRequest+1; i++ {
			p.FileFields = append(p.FileFields, fmt.Sprintf("uploadedfile%d", i))
		}

		var uploadedMagics []string

		err = r.ParseMultipartForm(32 << 20)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "upload", false, "action", "multipart form parsing", "extra", err.Error())
			p.ErrorMessage = "Limit of maximum files readed. Please try again lated."
			if err = tmpl.Execute(w, p); err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			return
		}

		for i := 1; i < cfg.MaxUploadsPerRequest+1; i++ {
			file, handler, err := r.FormFile(fmt.Sprintf("uploadedfile%d", i))
			if err != nil {
				//kitlogger.Log("client", detectRemoteIp(r), "request", r.URL, "upload", false, "action", "form parsing", "extra", fmt.Sprintf("uploadedfile%d", i))
				continue
			}
			defer file.Close()

			// in case of a temporaty file was created, make sure
			// this file will be removed, in case of an error, we continue
			mfile, err := handler.Open()
			if err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "upload", false, "action", "temporary file opening", "extra", err.Error())
			} else {
				switch t := mfile.(type) {
				case *os.File:
					defer t.Close()
					defer os.Remove(t.Name())
				default:
				}
			}

			username := getStringFromSession(r, "User")
			if username == "" {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to get username from session", "name", sessionName, "field", "User")
				http.Error(w, "No username found", http.StatusInternalServerError)
				return
			}

			clientip := detectRemoteIP(r)
			if clientip == "" {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "unable to extract client ip", "extra", err)
				http.Error(w, "Invalid remote address", http.StatusInternalServerError)
				return
			}

			fd, err := filer.StoreUpload(handler.Filename, cfg.ExpiryDays, username, clientip, file)
			if err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to store file", "extra", err)
				p.ErrorMessage = "Limit of maximum files readed. Please try again lated."
				if err = tmpl.Execute(w, p); err != nil {
					kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				return
			}
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "upload", true, "magiccode", fd.MagicCode, "filename", handler.Filename)

			p.Files = append(p.Files, *fd)

			uploadedMagics = append(uploadedMagics, fd.MagicCode)

			// increment the prometheus upload counter for each file
			metricsTotalUploads.Inc()

		}

		// Store the magic codes of all uploaded files in a session variable
		session.Values["UploadedFiles"] = uploadedMagics
		if err := session.Save(r, w); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to save session", "name", sessionName, "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err != nil {
			if err = tmpl.Execute(w, p); err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		http.Redirect(w, r, "/upload", http.StatusFound)

	}).Methods("POST")

	mux.HandleFunc("/content", func(w http.ResponseWriter, r *http.Request) {
		username := getStringFromSession(r, "User")
		if username == "" {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to get username from session", "name", sessionName, "field", "User")
			http.Error(w, "No username found", http.StatusInternalServerError)
			return
		}
		if !authMgr.IsAdmin(username) {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "user not authorized", "username", username)
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		tmpl, err := ace.Load("templates/content", "", &aceOpts)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var files structs.SortedFileDetails
		err = filer.GetFiles(&files)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		p := ContentPage{Files: files}
		p.CsrfField = csrf.TemplateField(r)
		p.FqdnWebRoot = getDownloadURL(r)

		if err = tmpl.Execute(w, p); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}).Methods("GET")

	mux.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		var p LoginPage
		p.CsrfField = csrf.TemplateField(r)

		template, err := ace.Load("templates/login", "", &aceOpts)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to load template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if r.Method == "POST" {

			session, err := sessionStore.Get(r, sessionName)
			if err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to get session", "name", sessionName, "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			username := strings.ToLower(r.FormValue("username"))
			password := r.FormValue("password")

			err = authMgr.VerifyUser(username, password)
			if err == nil {
				session.Values["User"] = username
				if err := session.Save(r, w); err != nil {
					kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to save session", "name", sessionName, "extra", err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "login", true, "username", username, "iadmin", authMgr.IsAdmin(username))
				http.Redirect(w, r, "/upload", http.StatusFound)
				return
			}

			// wipe possible existing sessions
			delete(session.Values, "User")
			if err := session.Save(r, w); err != nil {
				kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to save session", "name", sessionName, "extra", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "login", false, "error", "invalid username or password", "username", username, "extra", err)
			p.ErrorMessage = "Invalid username or password."
		}

		if err = template.Execute(w, p); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to execute template", "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})

	mux.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		// Remove the user details from auth session
		session, err := sessionStore.Get(r, sessionName)
		if err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to get session", "name", sessionName, "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		username := getStringFromSession(r, "User")
		if username != "" {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "logout", true, "username", username)
		}

		delete(session.Values, "User")
		if err := session.Save(r, w); err != nil {
			kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "error", "failed to save session", "name", sessionName, "extra", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Redirect to login page again.
		http.Redirect(w, r, "/login", http.StatusFound)
	})

	mux.PathPrefix("/site_media").Handler(http.FileServer(http.Dir("./templates/")))

	prometheus.MustRegister(metricsTotalDownloads)
	prometheus.MustRegister(metricsTotalUploads)
	http.Handle("/metrics", prometheus.Handler())
	go func() {
		kitlogger.Log("prometheus", true, "listen-address", cfg.ListenAddressPrometheus)

		srv := &http.Server{
			Addr:         cfg.ListenAddressPrometheus,
			ReadTimeout:  60 * time.Second,
			WriteTimeout: 60 * time.Second,
		}
		if err := srv.ListenAndServe(); err != nil {
			kitlogger.Log("prometheus", false, "error", "failed to start prometheus exporter", "address", cfg.ListenAddressPrometheus, "extra", err)
			os.Exit(1)
		}
	}()

	n := negroni.Classic()
	n.Use(negroni.HandlerFunc(verifyFileStorage))
	n.Use(negroni.HandlerFunc(verifyUser))

	// enable csrf protection, if not disabled in configuration
	if cfg.CsrfDisabled != true {
		n.UseHandler(csrf.Protect([]byte(cfg.CsrfToken), csrf.Secure(!cfg.CsrfInsecure))(mux))
	} else {
		n.UseHandler(mux)
	}

	kitlogger.Log("webdisk", true, "listen-address", cfg.ListenAddress, "version", version)
	srvm := &http.Server{
		Addr:         cfg.ListenAddress,
		ReadTimeout:  600 * time.Second,
		WriteTimeout: 600 * time.Second,
		ErrorLog:     log.New(os.Stdout, "[httpd] ", log.Ldate|log.Ltime),
		Handler:      n,
	}
	if err := srvm.ListenAndServe(); err != nil {
		kitlogger.Log("webdisk", false, "error", "failed to start server", "address", cfg.ListenAddress, "extra", err)
		os.Exit(1)
	}
}

func getDownloadURL(r *http.Request) string {
	if cfg.ApplicationURL != "" {
		kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "application-url", cfg.ApplicationURL)
		return cfg.ApplicationURL
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	uri := fmt.Sprintf("%s://%s", scheme, r.Host)
	kitlogger.Log("client", detectRemoteIP(r), "request", r.URL, "application-url", uri)

	// save this url for later usage, no need to detect this everytime
	cfg.ApplicationURL = uri
	return uri
}

func detectRemoteIP(r *http.Request) string {
	// Walk over all possible headers and check if we can extract a value
	for _, h := range []string{"X-Forwarded-For", "X-Real-Ip"} {
		address := r.Header.Get(h)
		if address != "" {
			return address
		}
	}

	// No usable HTTP header found, return the IP from request
	host, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return ""
	}

	return host
}
