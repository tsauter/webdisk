
NAME=webdisk
MODULES=main.go

run:
	go run ${LDFLAGS} ${MODULES}

runlocalhost:
	go run ${LDFLAGS} ${MODULES} --listen 127.0.0.1:8080 --listen-prometheus 127.0.0.1:9080 --insecure

install: test
	go install ${LDFLAGS} -v

build: test
	go build ${LDFLAGS} -v -o ${NAME}.exe ${MODULES}

fmt:
	go fmt

test:
	go test

# https://github.com/golang/lint
lint:
	golint

# https//godoc.org/golang.org/x/tools/cmd/vet
vet:
	go vet

godep:
	godep save

clean:
	del ${NAME}.exe

distclean: clean

