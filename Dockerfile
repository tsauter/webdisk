FROM debian:jessie
MAINTAINER Thorsten Sauter

ENV VERSION=0.10

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y curl git

RUN curl https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz --output /go1.8.3.linux-amd64.tar.gz --silent && tar -C /usr/local -xzf /go1.8.3.linux-amd64.tar.gz && ln -s /usr/local/go/bin/go /usr/local/bin/go && rm /go1.8.3.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin

RUN adduser --system diskowner

ENV GOPATH=/goenv
ENV PATH=$PATH:$GOPATH:/bin
WORKDIR /webdisk
RUN mkdir -p /goenv

RUN go get github.com/goreleaser/goreleaser
RUN mkdir -p /goenv/src && cd /goenv/src && git clone https://gitlab.com/tsauter/webdisk.git && cd webdisk && /goenv/bin/goreleaser --skip-publish

RUN cp -v `find /goenv/src/webdisk/dist/ -type f -name webdisk` /goenv/bin
RUN cp -ar /goenv/src/webdisk/templates /webdisk/templates


ENTRYPOINT ["/goenv/bin/webdisk"]
CMD ["-directory=/webdisk/data", "-listen=:8080", "-listen-prometheus=:9080", "-userdb-file=/webdisk/etc/users.json"]

#docker run -n webdisk -p 80:8080 -p 9080:9080 -v /data/webdisk:/webdisk tsauter/webdisk

EXPOSE 8080
EXPOSE 9080
VOLUME /webdisk

